package deloitte.academy.lesson01.run;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import deloitte.academy.lesson01.array.ArrayBoolean;
import deloitte.academy.lesson01.array.ArrayCourse;
import deloitte.academy.lesson01.array.ArrayInt;
import deloitte.academy.lesson01.array.ArrayString;

public class Run {

	public static List<String> alumno = new ArrayList<>();
	public static void main(String[] args) {		
		ArrayInt aInt = new ArrayInt();
		ArrayString aStr = new ArrayString();
		ArrayBoolean aBoo = new ArrayBoolean();
		ArrayCourse aCou = new ArrayCourse();
		
		//Arreglo de n�meros
		int[] aNum = {1,3,5,6,2,7,10,11,20,2};
		Integer num  = 0;
		List<Integer> num2 = new ArrayList<Integer>();
		
		//Opcion consola
		Scanner consola = new Scanner(System.in);
		
		// Opci�n
		int opcion = 0;
		
		//Palabras clave
		String resultado = "";
		String[] palabras = {"Hola", "Pez", "jiji", "Perritos"};
		
		
		//Boolean
		Boolean[] listaB = {true, false, false, false, true, true, true, true};
		List<String> listaTrue = new ArrayList<String>();
		List<String> listaFalse = new ArrayList<String>();
		
		//Curso
		
		
		System.out.println("Selecciona la opci�n deseada:\n1)Array Int.\n2)Array String.\n3)Array Boolean.\n"
				+ "4)Array Course.\n");
		
		opcion = consola.nextInt();
		
		
		/* Men�:
		 * Opci�n 1: Array int
		 * Opci�n 2: Array String
		 * Opci�n 3: Array Boolean
		 * Opci�n 4: Array Course
		 */
		if(opcion == 1) {
			//N�mero mayor
			num = aInt.numMayor(aNum);
			System.out.println("El n�mero mayor del arreglo es: " + num);
			
			//N�mero menor
			num = aInt.numMenor(aNum);
			System.out.println("El n�mero menor del arreglo es: " + num);
			
			//Lista n�meros repetidos
			num2 = aInt.numRepetido(aNum);
			System.out.println("Los n�meros repetidos son: " + num2);
			
		}else if(opcion == 2) {
			//Palabra clave
			String clave = "Pez";
			resultado = aStr.palabraClave(palabras, clave);
			System.out.println("Resultado al buscar la palabra clave "+ clave +": " + resultado);
			
		}else if(opcion == 3) {
			//Lista true
			listaTrue = aBoo.valoresTrue(listaB);
			System.out.println(listaTrue);
			
			//Lista false
			listaFalse = aBoo.valoresFalse(listaB);
			System.out.println(listaFalse);
		}else if(opcion == 4){
			
			//Agregar alumno
			alumno = aCou.agregarAlumno("Jennifer", "17/03/1996");
			alumno = aCou.agregarAlumno("Erik", "30/01/1996");
			alumno = aCou.agregarAlumno("Luis", "29/01/1996");
			alumno = aCou.agregarAlumno("Carlo", "28/01/1996");
			
			System.out.println("Lista de alumnos:\n" + alumno + "\n");
			
			//Eliminar alumno
			alumno = aCou.eliminarAlumno("Luis");
			System.out.println("Lista con elementos eliminados: \n" + alumno);
			
			//Ordenar alumnos
			alumno = aCou.ordenarAlumnos();
			System.out.println("Lista ordenada:\n" + alumno);
			
		}else {
			System.out.println("Error, solo puedes ingresar n�meros enteros del 1 al 4");
		}
		

	}

}
