package deloitte.academy.lesson01.array;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que permite buscar un palabra clave dentro del arreglo
 * @author jenrubio
 *
 */
public class ArrayString {
	//Variable para regresar la posici�n de la palabra clave
	String palabraEnc = "";
	
	//Declaraci�n del Logger
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	
	/**
	 * M�todo para buscar una palabra Clave dentro de un arreglo. 
	 * @param palabras: Arreglo tipo String que contiene varias palabras.
	 * @param palabraClave: String que contiene una palabra clave ingresada por el usuario.
	 * @return palabraEnc: regresa OK si encontr� la palabra clave o KO si no fue encontrada.
	 */
	public String palabraClave(String[] palabras, String palabraClave) {
		int valor = 0;
		try {
			for(int i = 0; i < palabras.length; i++) {
				if(palabraClave == palabras[i]) {
					valor = 1;
				}
			}
			
			if(valor == 1) {
				palabraEnc = "\nPalabra clave encontrada";
			}
			else {
				palabraEnc = "\nPalabra clave NO encontrada";
			}
			LOGGER.info("Palabra clave: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		
		return palabraEnc;
	}
	


}
