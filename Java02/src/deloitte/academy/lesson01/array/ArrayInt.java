package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase para obtener el n�mero mayor, menor, repetido contenido en el arreglo
 * @author jenrubio
 */

public class ArrayInt {
	//Variable return
	Integer numero = 0;
	
	//Declaraci�n del Logger
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	
	
	/**
	 * M�todo que determina cu�l es el n�mero mayor de un arreglo.
	 * @param numeros: Arreglo en el cual los valores que contiene son del tipo int
	 * @return numero: el n�mero mayor
	 */
	public Integer numMayor(int[] numeros){
		numero = numeros[0];
		
		try {
			for(int num : numeros) {
				if(num > numero) {
					numero = num;
				}
			}
			LOGGER.info("N�mero Mayor: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return numero;
	}
	
	/**
	 * M�todo que determina el n�mero menor del arreglo ingresado.
	 * @param numeros: Arreglo en el cual los valores que contiene son del tipo int
	 * @return numero: n�mero menor del arreglo
	 */
	public Integer numMenor(int[] numeros) {
		numero = numeros[0];
		
		try {
			for(int num : numeros) {
				if(num < numero) {
					numero = num;
				}
			}
			LOGGER.info("N�mero Menor: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return numero;
	}
	
	/**
	 * M�todo que permite determinar qu� n�meros se repiten en un arreglo.
	 * @param numeros: Arreglo en el cual los valores que contiene son del tipo int
	 * @return numero
	 */
	public List<Integer> numRepetido(int[] numeros) {
		List<Integer> repetidos = new ArrayList<Integer>();

		try {
			for(int i = 0; i < numeros.length; i++) {
				for(int j = i+1; j < numeros.length; j++) {
					if(numeros[i] == numeros[j]) {
						int num = numeros[j];
						repetidos.add(num);
					}
				}
			}
			LOGGER.info("N�meros Repetidos: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return repetidos;
	}
	
}
