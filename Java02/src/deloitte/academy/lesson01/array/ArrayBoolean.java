package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que permite separar los true o false una lista principal se debe de retornar 
 * dos listas una donde se incluyan solo true y otras false.
 * @author jenrubio
 */

public class ArrayBoolean {
	//Declaraci�n del Logger
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
		
	//Declaraci�n de listas
	List<String> listaTrue = new ArrayList<String>();
	List<String> listaFalse = new ArrayList<String>();
	
	/**
	 * M�todo para separar los valores tipo "true" de un arreglo
	 * @param arreglo: Array tipo boolean
	 * @return listaTrue: Lista con los valores true obtenidos del arreglo.
	 */
	public List<String> valoresTrue(Boolean[] arreglo){
		try {
			for(int i = 0; i < arreglo.length; i++) {
				if(arreglo[i] == true) {
					listaTrue.add("true");
				}
			}
			LOGGER.info("Lista true: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return listaTrue;
	}
	
	/**
	 * M�todo para separar los valores tipo "false" de un arreglo
	 * @param arreglo: Arreglo tipo Boolean 
	 * @return listaFalse: regresa los false que encontr� en el arreglo inicial
	 */
	public List<String> valoresFalse(Boolean[] arreglo){
		try {
			for(int i = 0; i < arreglo.length; i++) {
				if(arreglo[i] == false) {
					listaFalse.add("false");
				}
			}
			LOGGER.info("Lista false: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return listaFalse;
	}
	

}
