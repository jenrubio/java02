package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase en la cual los atributo ser�n nombre, fecha 
 * Y permitir� agregar,remove,ordenar atributos a la lista
 * Y regresar� la lista con estos cambios
 * @author jenrubio
 */

public class ArrayCourse {
	//Declaraci�n del Logger
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());

	List<String> alumnosLista = new ArrayList<String>();
	
	
	/**
	 * M�todo para agregar un alumno con fecha
	 * @param nombre: (String) del alumno
	 * @param fecha: (String) fecha de nacimiento del alumno.
	 * @return alumnosLista: Lista actualizada de los alumnos.
	 */
	public List<String> agregarAlumno(String nombre, String fecha){
		try {
			alumnosLista.add(nombre.concat(" ").concat(fecha));
			LOGGER.info("Agregar alumno: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
				
		return alumnosLista;
	}
	
	/**
	 * M�todo para eliminar un alumno espec�fico
	 * @param nombre: Nombre del alumno a eliminar.
	 * @return alumnoEliminar: Lista actualizada sin los alumnos eliminados.
	 */
	public List<String> eliminarAlumno( String nombre){
		List<String> alumnoEliminar = new ArrayList<>();
		alumnoEliminar = alumnosLista;
		
		try {
			for (String nomb : alumnoEliminar) {
				if(nomb.contains(nombre)) {
					alumnoEliminar.remove(nomb);
					break;
				}
			}
			LOGGER.info("Eliminar alumno: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return alumnoEliminar;
	}
	
	/**
	 * M�todo para organizar la lista acorde al nombre del alumno.
	 * @return alumnosLista: Lista ordenada
	 */
	public List<String> ordenarAlumnos(){
		try {
			alumnosLista.sort(null);
			LOGGER.info("Ordenar lista: OK");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return alumnosLista;
	}
}
